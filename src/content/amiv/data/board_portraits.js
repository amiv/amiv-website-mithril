// Contains static information about the current board.
// Markdown can be used to style the text.

import { boardRoles } from './board_roles';

import boardImage from '../images/board/hs19.jpg';
import presidentImage from '../images/board/Luca.jpg';
import quaestorImage from '../images/board/Lea.jpg';
import kulturImage1 from '../images/board/Max.jpg';
import kulturImage2 from '../images/board/Alex.jpg';
import kulturImage3 from '../images/board/Pey.jpg';
import erImage1 from '../images/board/Leon.jpg';
import erImage2 from '../images/board/Svenja.jpg';
import hopoImageITET from '../images/board/Markus.jpg';
import hopoImageMAVT from '../images/board/Fabi.jpg';
import informationImage from '../images/board/Fran.jpg';
import infrastructureImage from '../images/board/Nic.jpg';
import itImage from '../images/board/Luzi.jpg';

const boardPortraits = [
  {
    image: presidentImage,
    showRoles: true,
    portraits: [
      {
        role: boardRoles.president,
        name: 'Luca Dahle',
        description: {
          de:
            `"Luca, was muss unbedingt in die Beschreibung über dich?" "Luca ist heiss und reich." 
            
            Im AMIV Büro gilt Luca eigentlich schon als Urgestein. Bereits seit 2014 ist er dort anzutreffen und kennt den AMIV wie wohl nur wenige andere. Dies rührt nicht zuletzt daher, dass er zu seiner Zeit bereits Infra-Vorstand war, und er praktisch rund um die Uhr im Büro anzutreffen ist, meist mit einer Hülse in der Hand gemütlich jassend («Warum chunnsch nöd mitem Buur du A****loch?») oder, seit er sein neues Amt innehat, sogar auch am Schreibtisch arbeitend. So bleibt nichts, was im AMIV passiert, von ihm unbemerkt, was ihm in seiner Position als neues Oberhaupt sehr gelegen kommt. Das ermöglicht ihm mit seinem ausgeprägten Gerechtigkeitssinn und seinem Flair für Schlagermusik im Hause AMIV Frieden und Freude walten zu lassen.`,
        },
      },
    ],
  },
  {
    image: quaestorImage,
    showRoles: true,
    portraits: [
      {
        role: boardRoles.quaestor,
        name: 'Lea Kotthoff',
        description: {
          de:`Lea, auch als Leanidas oder „die Kasse“ bekannt, hat ihre AMIV-Karriere im LIMES gestartet. Den Quästur-Posten hat sie dort mit viel Bravour gemeistert. Doch die Limes Kasse war ihr nicht gross genug, weshalb sie den Aufstieg zur AMIV Quästorin gewagt hat. Diesen Job verrichtet sie mit solchem Engagement, dass sie quasi schon mit ihm zu einem verschmolzen ist. Daher stammt auch der Name: Lea, die Kasse. 
            
            Neben dem AMIV ist Lea auch oft im ASVZ oder beim Leichtathletik Training vorzufinden. Zudem gibt sie im Winter Snowboardunterricht und wird im Sommer von Reality-TV Stars auf Kaffees eingeladen. 
            
            Fun Fact: Lea hat noch nie Spongebob gesehen, jedoch konnte sie die Zahl Pi bis zur 100. Nachkommastelle auswendig.`,
        },
      },
    ],
  },
  {
    image: itImage,
    showRoles: true,
    portraits: [
      {
        role: boardRoles.it,
        name: `Luzian Bieri`,
        description: {
          de:
          `Seine Anfänge im AMIV hatte Luzi bereits am ESW. Schnell war klar, dass er für den AMIV wie geschaffen war (sein Kürzel ist luziBIER, for real?!). Er trat der Brauko bei und wurde Kulturi der Leiden (schafft), womit er sich im AMIV schnell einen Namen machte. Auch der Kontakt drückt er als PR Verantwortlicher seinen Stempel auf und meistert diese Aufgabe mit Bravour und einer unglaublicher Effizienz, wie man sie von einem Berner kaum gewohnt ist.
          
          Doch das war Luzi noch alles nicht genug, weshalb er sich zusätzlich den Posten als Quästor des AMIV aufhalste. Dies alles führt dazu, dass Luzi 24/7 im Büro anzutreffen ist und schon fast zum Inventar gehört. Man munkelt dass er immer wieder seine pyromaische Ader auslebt und allen möglichen Materialien einem Feuertest unterzieht, weshalb zu hoffen ist, dass das Büro Luzis Amtszeit übersteht.
          
          Da er noch nicht genug vom AMIV hatte, entschied er sich ein weiteres Semester als IT-Vorstand weiterzumachen.`
        },
      },
    ],
  },
  {
    image: informationImage,
    showRoles: true,
    portraits: [
      {
        role: boardRoles.information,
        name: `Francesca Burlini`,
        description: {
          de:
            `Hi guys, ich heisse Fran. Zu meinen Lieblingshobbies gehören Jäggitöggele und Besteigen der Gondel. Ich habs auch schon nach einem halben Jahr auf die Employee-of-the-month Tafel im AMIV-Büro geschafft #goals. Aber jetzt Spass beiseite. Im AMIV habe ich schon viel geleistet: Ich habe in vielen OKs wie beim Pokerturnier, Beachvolleyball und der Ersti-Rallye mitgemacht und helfe sonst auch überall mit. Ich freue mich euch in Zukunft über Social Media auf neustem Stand des AMIVs zu halten. Mit Polls, Countdowns und ganz vielen tollen Pics! 

            .

            .

            #frannounce #dieschwachenimmts #nasty #wegendersicherheit #whereisashley #tümmersvakumiere #cringe #bliboischläbe #nastyboy #ichwettkeinnastynitchtrinke #sick #lowkeynasty #frannygranny #nödimpressed #hodenkobold #asemotionallystableasanikeatable #gsi #sänkiu`,
        },
      },
    ],
  },
  {
    image: kulturImage1,
    showRoles: true,
    portraits: [
      {
        role: boardRoles.eventPlanning,
        name: `Max Aspect`,
        description: {
          de:
            `in ungewöhnlicher Weg führte Max nach Zürich. Mr. Cosmopolitan ist in Paris geboren, hat in Düsseldorf den Kindergarten besucht, wieder zurück in Paris das französische Schulsystem getestet und zu guter Letzt in Zürich die Schule abgeschlossen. 
            
            Angekommen an der ETH führte für den Kulturi aus Leidenschaft kein Weg am AMIV vorbei. Schnell gab es kaum eine Kommission im AMIV die noch vor Max sicher war. Sein Engagement beim blitz, Braukommission, Irrational Co., EESTEC und RandomDudes dienten ihm als perfekte Vorbereitung für seine Aufgaben als Kulturvorstand. Es ist erstaunlich, dass er neben all diesen Tätigkeiten sogar ab und zu noch Zeit für sein Maschinenbaustudium findet.`,
        },
      },
    ],
  },
  {
    image: kulturImage2,
    showRoles: false,
    portraits: [
      {
        role: boardRoles.eventPlanning,
        name: `Alexandra Lyons`,
        description: {
          de:
            `Alex‘ AMIV-Karriere ist wie aus dem Bilderbuch. Nachdem sie beim Ersti-Weekend mit ihrem Team gewonnen hat, wurde sie so schnell ein fixer Bestandteil des besten Fachvereins der Welt, dass sie nicht mehr wegzudenken wäre. Sie ist sogar Motiv eines Stickers, der Tassen, Wände und Tische des AMIV-Büros ziert. 
            
            Nachdem Alex sowieso bei quasi bei allen AMIV Events dabei ist, ändert sich für sie als Kulturvorstand da gar nicht so viel. Ausser natürlich, dass sie die Events jetzt selbst organisiert. Bei diesen, aber auch bei Büropartys, Tichu-Runden und neuerdings Vorstandssitzungen steigert sie mit ihrer lustigen und gesprächigen Art den Unterhaltungsfaktor enorm. Somit ist gute Stimmung vorprogrammiert, wenn sie dabei ist!`,
        },
      },
    ],
  },
  {
    image: kulturImage3,
    showRoles: false,
    portraits: [
      {
        role: boardRoles.eventPlanning,
        name: `Pelayo Garcia`,
        description: {
          de: `Pey – voll der Typ, voll on fire. Nach dem mutigen Schritt soweit fern ab der Heimat zu studieren – er stammt aus dem Land der Lockenköpfe (Spanien) – fand er sich schnell in Zürich zurecht. Über das Gärtankputzen bei der Brauko kam er in den AMIV und gewann dabei viele neue Freunde. Mit diesen übernahm er schnell einige OKs und verbandelte sich mit dem Kulturvorstand. Peys Fähigkeit gehen von einem guten Organisationstalent bis hin zum Adaptieren von regionalen Dialekten.`,
        },
      },
    ],
  },
  {
    image: hopoImageITET,
    showRoles: true,
    portraits: [
      {
        role: boardRoles.universityPolicy,
        name: `Markus Niese`,
        description: {
          de:
            `Unser Markus ist die einzige Repräsentation des ITET im diesjährigen Vorstand, allein das kann nicht einfach zu tragen sein. Sein Engagement aber entspricht dem von mindestens drölf Leuten! Ob bei spontanen Aktionen wie ein Hilferuf von Max ("PLÖTZLICH muss ich mein Klavier transportieren, kann mir jemand helfen??") oder einer undercover Reportage für den blitz aber auch bei langwierigen Aufgaben wie Planen von PVKs und Semesterevaluationen oder der Betreuung bei Physik Meisterschaften (als Elektroniktyp???) – mit ihm hat man in jedem Fall zweimaldrölf motivierte, hilfsbereite Hände zur Seite. 
            
            Das HoPo-ITET Team zählt ihn schon seit Anbeginn seines Studiums zu seinem Goldschatz und nun als sein Anführer wird Elektrotechnik sicher bald zum beliebtesten Studiengang der Welt.`,
        },
      },
    ],
  },
  {
    image: hopoImageMAVT,
    showRoles: true,
    portraits: [
      {
        role: boardRoles.universityPolicy,
        name: `Fabian Neumüller`,
        description: {
          de: `Fabians Anfänge im AMIV waren beim Ersti-Weekend. Seither ist er ein eifriger HoPoler und hat da schon alles gemacht, was es so gibt: Semestersprecher, PVK Organisator und Mitglied einer Berufungskommssion. Somit ist er perfekt auf seine Aufgabe als HoPo-Vorstand vorbereitet. 
          
          Ansonsten ist Fabian auch gerne für alle möglichen verrückten Aktionen zu haben. Wie etwa mal eben bei einem Marathon in Stockholm mitzumachen, obwohl er bis drei Wochen davor nie mehr als 12 km am Stück gelaufen ist. So konnte er Wetten gegen alle seine Kollegen gewinnen, die ihm das nicht zugetraut hätten. Oder im Winter auf lange Skitouren zu gehen, was ihm dann noch mehr Spass macht. Danach geniesst gerne ein Bier. Natürlich nur nach bayrischem Reinheitsgebot, wie es sich für einen Münchner gehört.`,
        },
      },
    ],
  },
  {
    image: erImage1,
    showRoles: true,
    portraits: [
      {
        role: boardRoles.externalRelations,
        name: `Leon Hinderling`,
        description: {
          de: `Man kann sich darüber streiten, welcher ER Vorstand die prominentere Frisur hat, diese Frage soll hier aber auch gar nicht ausdiskutiert werden. Was allerdings klar ist: Leon ist voll an seinem Platz angekommen. Seit dem Beginn seiner ETH-"Karriere" ist er im Aufenthaltsraum anzutreffen und bringt seine Legi zum dampfen. Immer gemütlich und gechillt führt er nun das ER Team als amiv Vorstand, nachdem er so lange mit dabei war. Seinen Platz auf dem Sofa im Aufenthaltsraum hat er gegen ein Sofa im Büro ausgetauscht. Und wie  es isch für einen wahren Sparfuchs gehört  steht an seinem neuen Arbeitsplatz seine eigene Palette Mate`,
        },
      },
    ],
  },
  {
    image: erImage2,
    showRoles: true,
    portraits: [
      {
        role: boardRoles.externalRelations,
        name: `Svenja Ruth`,
        description: {
          de:
            `Schon als Ersti war Svenja im AMIV weit bekannt. Ob in der Braukommission oder beim Organisieren von Kulturevents – sie war immer mit Herzblut dabei und wurde zu so etwas wie der Seele dieses Vereins. Und so ist es auch kein Wunder, dass sie schon vor Jahren im Organisationskomitee der Kontakt.15 oder beim Erstellen des heute noch gebrauchten Infobüchleins für Sponsoren reichlich Erfahrungen im ER-Ressort gesammelt hat. Mit diesen Erfahrungen und viel Kreativität möchte sie die Externen Beziehungen des AMIV ungeahnte Höhen erreichen lassen!`,
        },
      },
    ],
  },
  {
    image: infrastructureImage,
    showRoles: true,
    portraits: [
      {
        role: boardRoles.infrastructure,
        name: `Nicholas Doerk`,
        description: {
          de: `Aus dem schönsten Land der Erde kommend (nicht der Schweiz, sondern Bayern mit dem einzigartigen weiss-blauen Himmel) wurde er aus dem gemütlichen Leben gerissen und in den ungemütlichen ETH-Alltag geschmissen. Aber eine Sache hat ihm Heimatgefühle gewährt: die Wurst&Bier-Events vom AMIV.

Sein Rat an die Erstis: “Kommt vorbei, trinkt 'ne Pulle mit mir und spielt Tichu. Absolut sinnvoll investierte Zeit!” Aber Achtung – auch im Sommer bei 39°C trägt er lange dunkle Hosen mit Segelschuhen und Ballerinasocken. Das erwartet er auch von euch, wenn ihr zum Kartenspielen vorbeikommt.`,
        },
      },
    ],
  },
];

export { boardPortraits, boardImage };