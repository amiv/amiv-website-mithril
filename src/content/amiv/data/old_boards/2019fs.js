// Contains static information about a previous board.
// Markdown can be used to style the text.

import { boardRoles } from '../board_roles';
import boardImage from '../../images/board/old/fs19.jpg';

/**
 * Board information for autumn semester 2018 (HS18)
 */
export default {
  year: 2019,
  semester: 'fs',
  image: boardImage,
  members: [
    {
      role: boardRoles.president,
      name: 'Antonia Mosberger',
    },
    {
      role: boardRoles.quaestor,
      name: 'Luzian Bieri',
    },
    {
      role: boardRoles.it,
      name: 'vakant',
    },
    {
      role: boardRoles.information,
      name: 'Patricia Schmid',
    },
    {
      role: boardRoles.eventPlanning,
      name: 'Ian Boschung',
    },
    {
      role: boardRoles.eventPlanning,
      name: 'Betty Lory',
    },
    {
      role: boardRoles.eventPlanning,
      name: 'Max Aspect',
    },
    {
      role: boardRoles.universityPolicyItet,
      name: 'Lioba Heimbach',
    },
    {
      role: boardRoles.universityPolicyMavt,
      name: 'Julia Jäggi',
    },
    {
      role: boardRoles.externalRelations,
      name: 'Silvio Geel',
    },
    {
      role: boardRoles.externalRelations,
      name: 'Leon Hinderling',
    },
    {
      role: boardRoles.infrastructure,
      name: 'Lukas Eberle',
    },
  ],
};
